
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 window.Event = new Vue();

 Vue.component('teacher-list', require('./components/TeacherListForSubject.vue'));
 Vue.component('assign-subject', require('./components/AssignSubject.vue'));
 Vue.component('manage-semester', require('./components/ManageSemester.vue'));
 Vue.component('feeds', require('./components/Feeds.vue'));
 Vue.component('conversation-list', require('./components/ConversationList.vue'));
 Vue.component('conversation-reply', require('./components/ConversationReply.vue'));
 Vue.component('messaging-search', require('./components/MessagingSearch.vue'));
 Vue.component('semester-list', require('./components/SemesterList.vue'));
 Vue.component('semester-task', require('./components/SemesterTask.vue'));
 Vue.component('subject-list', require('./components/SubjectList.vue'));
 Vue.component('new-attendance', require('./components/NewAttendance.vue'));
 Vue.component('last-attendance', require('./components/LastAttendance.vue'));
 Vue.component('attendance-status', require('./components/AttendanceStatus.vue'));
 Vue.component('profile-activity-feeds', require('./components/ProfileActivityFeeds.vue'));
 Vue.component('teacher-change-password', require('./components/TeacherChangePassword.vue'));
 Vue.component('profile-search', require('./components/ProfileSearch.vue'));
 Vue.component('feeds-search', require('./components/FeedsSearch.vue'));
 Vue.component('semesters-search', require('./components/SemestersSearch.vue'));
 Vue.component('test-entry', require('./components/TestEntry.vue'));
 Vue.component('test-show', require('./components/TestShow.vue'));



 Vue.component('student-change-password', require('./components/student/StudentChangePassword.vue'));
 Vue.component('student-feeds', require('./components/student/StudentFeeds.vue'));
 Vue.component('student-messaging-search', require('./components/student/StudentMessagingSearch.vue'));
 Vue.component('student-conversation-list', require('./components/student/StudentConversationList.vue'));
 Vue.component('student-conversation-reply', require('./components/student/StudentConversationReply.vue'));
 Vue.component('student-semester-list', require('./components/student/StudentSemesterList.vue'));
 Vue.component('student-semester-task', require('./components/student/StudentSemesterTask.vue'));
 Vue.component('student-subject-list', require('./components/student/StudentSubjectList.vue'));
 Vue.component('student-attendance-status', require('./components/student/StudentAttendanceStatus.vue'));
 Vue.component('student-profile-activity-feeds', require('./components/student/StudentProfileActivityFeeds.vue'));
 Vue.component('student-profile-search', require('./components/student/StudentProfileSearch.vue'));
 Vue.component('student-feeds-search', require('./components/student/StudentFeedsSearch.vue'));
 Vue.component('student-semesters-search', require('./components/student/StudentSemestersSearch.vue'));
 Vue.component('student-test-show', require('./components/student/StudentTestShow.vue'));



 const app = new Vue({
     el: '#app',
     data: window.teacher,
 });
