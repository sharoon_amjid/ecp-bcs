@extends('admin.layouts.app')

  @section('content')
  <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add Student</h4>
                    </div>
                    <div class="content">
                        <form method="post" action="{{route('register.student.submit')}}">
                          {{csrf_field()}}
                          <div class="row">
                              <div class="col-md-4">
                                  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                      <label for="name">Name</label>

                                      <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>

                                      @if ($errors->has('name'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('name') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group{{ $errors->has('college_id') ? ' has-error' : '' }}">

                                      <label for="email">College ID#</label>

                                      <input id="college_id" type="text" class="form-control" name="college_id" value="{{ old('college_id') }}" required>

                                      @if ($errors->has('college_id'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('college_id') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">

                                      <label for="gender">Gender</label>

                                      <select id="gender" class="form-control" name="gender">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                      </select>

                                      @if ($errors->has('gender'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('gender') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('batch') ? ' has-error' : '' }}">

                                      <label for="batch">Batch</label>

                                      <select id="batch" class="form-control" name="batch">
                                        @foreach ($batches as $batch)
                                          @if(old('batch') == $batch->id)
                                           <option value="{{ $batch->id }}" selected>{{ $batch->display_name }}</option>
                                           @else
                                           <option value="{{$batch->id}}">{{$batch->display_name}}</option>
                                           @endif
                                        @endforeach
                                      </select>

                                      @if ($errors->has('batch'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('batch') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                      <label for="email">Email Address</label>

                                      <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                      @if ($errors->has('email'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('email') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                      <label for="password">Password</label>

                                      <input id="password" type="password" class="form-control" name="password" required>

                                      @if ($errors->has('password'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">

                                      <label for="password-confirm">Confirm Password</label>

                                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                  </div>
                              </div>
                          </div>
                            <button type="submit" id="teacher-register" class="btn btn-info btn-fill pull-right">Register</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
              </div>
          </div>



          <div class="row">
            <form class="" action="{{route('register.semester.submit')}}" method="post">
              {{csrf_field()}}
              <input class="btn btn-info" type="hidden" name="Store Semesters" value="Store Semesters">
            </form>
          </div>

          <div class="row">
            <form class="" action="{{route('register.subject.submit')}}" method="post">
              {{csrf_field()}}
              <input class="btn btn-info" type="hidden" name="Store Semesters" value="Store Subjects">
            </form>
          </div>
      </div>
  </div>
  @endsection
