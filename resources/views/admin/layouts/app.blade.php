<!doctype html>
<html lang="en">
<head>



  <script>
          window.Laravel = <?php echo json_encode([
              'csrfToken' => csrf_token(),
          ]); ?>
</script>

	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{config('app.name', 'Laravel')}}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('myprojectassets/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="{{ asset('myprojectassets/assets/css/animate.min.css') }}" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{ asset('myprojectassets/assets/css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>
    <!-- My Custom CSS -->
    <link href="{{ asset('myprojectassets/assets/css/mycustom.css') }}" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('myprojectassets/assets/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />

</head>
<body>

<div class="wrapper" id="app">
@include ('admin.layouts.partials._sidebar')
    <div class="main-panel">
      @include ('admin.layouts.partials._navbar')

        @yield('content')


        @include ('admin.layouts.partials._footer')
    </div>
</div>


</body>



</script>



<script src="{{ mix('js/app.js') }}"></script>
	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="{{ asset('myprojectassets/assets/js/bootstrap-checkbox-radio-switch.js') }}"></script>

	<!--  Charts Plugin -->
	<script src="{{ asset('myprojectassets/assets/js/chartist.min.js') }}"></script>

    <!--  Notifications Plugin    -->
  <script src="{{ asset('myprojectassets/assets/js/bootstrap-notify.js') }}"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="{{ asset('myprojectassets/assets/js/light-bootstrap-dashboard.js') }}
"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>



@if(session('status'))
<script type="text/javascript">
      $(document).ready(function(){



          $.notify({
              icon: "{{session('icon')}}",
              message: "{{session('status')}}"

            },{
                type: "{{session('type')}}",
                timer: 1000
            });

      });
  </script>
@endif


</html>
