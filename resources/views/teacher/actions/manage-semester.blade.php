@extends('admin.layouts.app')

  @section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <manage-semester></manage-semester>
        </div>
      </div>
  </div>
</div>
  @endsection
