@extends('admin.layouts.app')

  @section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card">
              <div class="header">
                  <h4 class="title">Teacher's Subject</h4>
              </div>
                <teacher-list></teacher-list>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
              <div class="header">
                  <h4 class="title">Assign Subject</h4>
              </div>
              <assign-subject></assign-subject>
          </div>
        </div>
      </div>
  </div>
</div>
  @endsection
