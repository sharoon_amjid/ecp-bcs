
<nav class="navbar navbar-default" id="navbar">
<div class="container-fluid">
  <div class="navbar-collapse collapse in">
    <ul class="nav navbar-nav navbar-mobile">
      <li>
        <button type="button" class="sidebar-toggle">
          <i class="fa fa-bars"></i>
        </button>
      </li>
      <li class="logo">
        <a class="navbar-brand" href="#"><span class="highlight">ECP v1</span> Student</a>
      </li>
      <li>
        <button type="button" class="navbar-toggle">
          <img class="profile-img" src="../../uploads/profile-pic/{{$user->profile_pic}}">
        </button>
      </li>
    </ul>
    @yield ('search-bar')
    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown notification warning">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <div class="icon"><i class="fa fa-comments" aria-hidden="true"></i></div>
          <div class="title">Unread Messages</div>
          <div class="count">99</div>
        </a>
        <div class="dropdown-menu">
          <ul>
            <li class="dropdown-header">Message</li>

            <li>
              <a href="#">
                <span class="badge badge-warning pull-right">2</span>
                <div class="message">
                  <img class="profile" src="https://placehold.it/100x100">
                  <div class="content">
                    <div class="title">"This feature will be added soon"</div>
                    <div class="description">Sharoon Amjid</div>
                  </div>
                </div>
              </a>
            </li>
            <li class="dropdown-footer">
              <a href="#">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </li>
          </ul>
        </div>
      </li>
      <li class="dropdown notification danger">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <div class="icon"><i class="fa fa-bell" aria-hidden="true"></i></div>
          <div class="title">System Notifications</div>
          <div class="count">10</div>
        </a>
        <div class="dropdown-menu">
          <ul>
            <li class="dropdown-header">Notification</li>
            <li>
              <a href="#">
                <span class="badge badge-danger pull-right">14</span>
                Salman Qasim Posted new question and tagged you.
              </a>
            </li>
            <li class="dropdown-footer">
              <a href="#">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </li>
          </ul>
        </div>
      </li>
      <li class="dropdown profile">
        <a href="/html/pages/profile.html" class="dropdown-toggle"  data-toggle="dropdown">
          <img class="profile-img" src="../../uploads/profile-pic/{{$user->profile_pic}}">
          <div class="title">Profile</div>
        </a>
        <div class="dropdown-menu">
          <div class="profile-info">
            <h4 class="username">{{$user->name}}</h4>
          </div>
          <ul class="action">
            <li>
              <a href="{{route('teacher.profile', $user->college_id)}}">
                Profile
              </a>
            </li>
            <li>
              <a href="#">
                <span class="badge badge-danger pull-right">5</span>
                My Inbox
              </a>
            </li>
            <li>
              <a href="#">
                Setting
              </a>
            </li>
            <li>
              <a href="{{route('logout')}}">
                Logout
              </a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>
</nav>
