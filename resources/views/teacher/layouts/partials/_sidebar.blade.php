<aside class="app-sidebar" id="sidebar">
  <div class="sidebar-header">
    <a class="sidebar-brand" href="#"><span class="highlight">ECP v1</span> Teacher</a>
    <button type="button" class="sidebar-toggle">
      <i class="fa fa-times"></i>
    </button>
  </div>
  <div class="sidebar-menu">
    <ul class="sidebar-nav">
      <li class="{{ Request::path() == 'teacher/semesters' ? 'active' : '' }}">
        <a href="{{route('teacher.semesters')}}">
          <div class="icon">
            <i class="fa fa-tasks" aria-hidden="true"></i>
          </div>
          <div class="title">Semesters</div>
        </a>
      </li>
      <li class="{{ Request::path() == 'teacher/feeds' ? 'active' : '' }}">
            <a href="{{route('teacher.feeds')}}">
              <div class="icon">
                <i class="fa fa-tasks" aria-hidden="true"></i>
              </div>
              <div class="title">Feeds</div>
            </a>
      </li>
      <li class="{{ Request::path() == 'teacher/messages' ? 'active' : '' }}">
        <a href="{{route('teacher.messages')}}">
          <div class="icon">
            <i class="fa fa-comments" aria-hidden="true"></i>
          </div>
          <div class="title">Messaging</div>
        </a>
      </li>
    </ul>
  </div>
  <div class="sidebar-footer">
    <ul class="menu">
      <li>
        <a href="/" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-cogs" aria-hidden="true"></i>
        </a>
      </li>
    </ul>
  </div>
</aside>
