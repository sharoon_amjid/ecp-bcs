@extends('teacher.layouts.app')
  @section('search-bar')
    <messaging-search></messaging-search>
  @endsection
  @section('content')
  <div class="app-messaging-container">
    <div class="app-messaging" id="collapseMessaging">
      <conversation-list></conversation-list>
      <conversation-reply></conversation-reply>
    </div>
  </div>
  @endsection
