@extends('teacher.layouts.app')
  @section('search-bar')
    <semesters-search></semesters-search>
  @endsection
  @section('content')
  <div class="app-messaging-container">
    <div class="app-messaging" id="collapseMessaging">
      <semester-list></semester-list>
      <semester-task></semester-task>
    </div>
  </div>
  @endsection
