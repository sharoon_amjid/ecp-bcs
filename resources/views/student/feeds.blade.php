@extends('student.layouts.app')
@section('search-bar')
  <student-feeds-search></student-feeds-search>
@endsection

  @section ('floating-button')
  <div class="btn-floating" id="help-actions">
    <div class="btn-bg"></div>
      <button type="button" class="btn btn-default btn-toggle" data-toggle="toggle" data-target="#help-actions">
        <i class="icon fa fa-plus"></i>
        <span class="help-text">Shortcut</span>
      </button>
      <div class="toggle-content">
        <form class="form-group" action="{{route('student.feeds.upload.submit', Auth::user()->college_id)}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="card card-mini">
          <div class="card-body no-padding table-responsive">
            <table class="table card-table">
              <tbody>
                <tr >
                  <td colspan="2">
                    <textarea name="description" placeholder="Write down your question" class="form-control"></textarea>
                  </td>
                </tr>
                <tr>

                  <td><input type="file" class="form-control" name="image_filename"></td>
                  <td><input type="submit" class="form-control btn btn-success" value="Upload"></td>
                </tr>
              </tbody>

            </table>
          </div>
        </div>
        </form>
      </div>
  </div>
  @endsection
  @section('content')
    <student-feeds image-path="{{public_path()}}"></student-feeds>
  @endsection
