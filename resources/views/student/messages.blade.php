@extends('student.layouts.app')
  @section('search-bar')
    <student-messaging-search></student-messaging-search>
  @endsection
  @section('content')
  <div class="app-messaging-container">
    <div class="app-messaging" id="collapseMessaging">
      <student-conversation-list></student-conversation-list>
      <student-conversation-reply></student-conversation-reply>
    </div>
  </div>
  @endsection
