@extends('student.layouts.app')
  @section('search-bar')
    <student-profile-search></student-profile-search>
  @endsection
  @section('content')
  <div class="row">
<div class="col-lg-12">
  <div class="card">
    <div class="card-body app-heading">
      <img class="profile-img" src="../../uploads/profile-pic/{{$users->profile_pic}}">
      <div class="app-title">
        <div class="title"><span class="highlight">{{$users->name}}</span></div>
        @if (!empty($users->title))
        <div class="description">{{$users->title}}</div>
        @endif
      </div>
    </div>
  </div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
  <div class="card card-tab">
    <div class="card-header">
      <ul class="nav nav-tabs">
        <li role="tab1" class="active">
          <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Profile</a>
        </li>
        @if (Auth::user()->id === $users->id)
        <li role="tab3">
          <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Account</a>
        </li>
        <li role="tab4">
          <a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Setting</a>
        </li>
        @endif
      </ul>
    </div>
    <div class="card-body no-padding tab-content">
      <div role="tabpanel" class="tab-pane active" id="tab1">
        <div class="row">

          <div class="col-md-3 col-sm-12">
            @if (!empty($about_me = $users->profile->about_me))
            <div class="section">
              <div class="section-title"><i class="icon fa fa-user" aria-hidden="true"></i> About Me</div>
              <div class="section-body __indent">{{$about_me}}</div>
            </div>
            @endif

            @if (!empty($education = $users->profile->education))
            <div class="section">
              <div class="section-title"><i class="icon fa fa-book" aria-hidden="true"></i> Education</div>
              <div class="section-body __indent">{{$education}}</div>
            </div>
            @endif

            @if(count($subjects) > 0)
            <div class="section">
              <div class="section-title"><i class="icon fa fa-book" aria-hidden="true"></i> Subjects</div>
              @foreach ($subjects as $subject)
              <div class="section-body __indent">{{$subject->display_name}}</div>
              @endforeach
            </div>
            @endif
          </div>

          <div class="col-md-9 col-sm-12">
            <div class="section">
              <div class="section-title">Activities</div>
              <div class="section-body">
                <student-profile-activity-feeds user-id="{{$users->id}}"></student-profile-activity-feeds>
              </div>
            </div>
          </div>
        </div>
      </div>
      @if (Auth::user()->id === $users->id)
      <div role="tabpanel" class="tab-pane" id="tab3">
        @include ('student.auth.change-password')
      </div>
      <div role="tabpanel" class="tab-pane" id="tab4">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile Information</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{route('student.save.profile.information')}}" enctype="multipart/form-data">
                          {{csrf_field()}}
                          <div class="row">
                            <div class="col-md-6">
                              <lable for="about_me"> About Me:</lable><br><br>
                              <textarea class="form-control" id="about_me" name="about_me" rows="8" placeholder="About your great personality">{{$users->profile->about_me}}</textarea>
                            </div>
                            <div class="col-md-6">
                              <lable for="education"> Education:</lable><br><br>
                              <textarea class="form-control" id="education" name="education" rows="8" placeholder="You acheived with your hardwork">{{$users->profile->education}}</textarea>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <lable for="education"> Change profile picture:</lable><br><br>
                              <input type="file" name="profile_pic" class="btn btn-sm btn-default">
                            </div>
                          </div>
                          <div class="row">
                            <input type="hidden" name="auid" value="{{$users->id}}">
                            <hr>
                            <div class="col-md-12">
                              <input type="submit" class="btn btn-sm btn-success" value="Update">
                            </div>
                          </div>

                        </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
      </div>
      @endif
    </div>
  </div>
</div>
</div>
  @endsection
