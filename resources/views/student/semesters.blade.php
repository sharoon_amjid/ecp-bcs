@extends('student.layouts.app')
@section('search-bar')
  <student-semesters-search></student-semesters-search>
@endsection
  @section('content')
  <div class="app-messaging-container">
    <div class="app-messaging" id="collapseMessaging">
      <student-semester-list></student-semester-list>
      <student-semester-task></student-semester-task>
    </div>
  </div>
  @endsection
