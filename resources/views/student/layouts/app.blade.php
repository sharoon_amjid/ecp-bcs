<!doctype html>
<html lang="en">
<head>

  <script>
          window.Laravel = <?php echo json_encode([
              'csrfToken' => csrf_token(),
          ]); ?>

          window.teacher = {
            url: '{{ config('app.url') }}',
            user: {
                id: {{ Auth::check() ? Auth::user()->id : 'null'}},
                college_id: {{ Auth::check() ? Auth::user()->college_id : 'null'}},
                authenticated: {{ Auth::check() ? 'true' : 'false' }},
            }
          }
</script>

	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>{{config('app.name', 'Laravel')}}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="{{ asset('myprojectassets/ft-assets/assets/css/vendor.css') }}" rel="stylesheet"/>
    <link href="{{ asset('myprojectassets/ft-assets/assets/css/flat-admin.css') }}" rel="stylesheet"/>
    <link href="{{ asset('myprojectassets/ft-assets/assets/css/theme/blue-sky.css') }}" rel="stylesheet"/>
    <link href="{{ asset('myprojectassets/ft-assets/assets/css/theme/blue.css') }}" rel="stylesheet"/>
    <link href="{{ asset('myprojectassets/ft-assets/assets/css/theme/red.css') }}" rel="stylesheet"/>
    <link href="{{ asset('myprojectassets/ft-assets/assets/css/theme/yellow.css') }}" rel="stylesheet"/>
    <link href="{{ asset('myprojectassets/ft-assets/assets/css/mycustomcss.css') }}" rel="stylesheet"/>

</head>

<body>

	<div class="app app-default" id="app">

    @include ('student.layouts.partials._sidebar')


	    <div class="app-container">

      @include ('student.layouts.partials._navbar')

          @yield ('floating-button')
          @yield ('content')

      @include ('student.layouts.partials._footer')
</div>
	</div>

</body>

	<!--   Core JS Files   -->
  <script src="{{ mix('js/app.js') }}"></script>

    <script src="{{ asset('myprojectassets/ft-assets/assets/js/vendor.js') }}" type="text/javascript"></script>
  <script src="{{ asset('myprojectassets/ft-assets/assets/js/app.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

			// Javascript method's body can be found in assets/js/demos.js
        	//demo.initDashboardPageCharts();

    	});
	</script>

</html>
