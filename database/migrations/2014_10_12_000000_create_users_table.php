<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('title')->nullable();
            $table->integer('college_id')->unique();
            $table->string('email')->unique();
            $table->string('api_token',60)->unique();
            $table->string('password');
            $table->enum('type',['teacher','student'])->default('student');
            $table->enum('gender',['male','female']);
            $table->text('profile_pic')->nullable();
            $table->integer('batch_id')->unassigned()->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('batch_id')->references('id')->on('batches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
