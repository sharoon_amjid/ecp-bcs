<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Batch extends Model
{

    protected $fillable = [
      'display_name', 'status', 'studying',
    ];

    public function semesters()
    {
      return $this->hasMany('App\Semester');
    }


    public function users()
    {
      return $this->hasMany('App\User');
    }

}
