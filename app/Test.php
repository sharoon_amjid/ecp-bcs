<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
  protected $fillable = [
      'test_type', 'total_marks', 'passing_marks', 'user_id', 'subject_id', 'batch_id', 'semester_id',
  ];

  public function testDetails()
  {
    return $this->hasMany('App\TestDetail');
  }
}
