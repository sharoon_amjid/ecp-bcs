<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServicProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('teacher.layouts.partials._navbar', \App\Http\ViewComposers\NavigationComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
