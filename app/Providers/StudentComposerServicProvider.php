<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class StudentComposerServicProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('student.layouts.partials._navbar', \App\Http\ViewComposers\StudentNavigationComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
