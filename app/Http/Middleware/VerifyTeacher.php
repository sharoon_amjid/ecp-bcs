<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyTeacher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
          if ($request->user()->type != 'teacher') {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
              return dd("You are not teacher");
            }
          }
        }

        return $next($request);
    }
}
