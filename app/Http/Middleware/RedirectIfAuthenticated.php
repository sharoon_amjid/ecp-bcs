<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      if (Auth::guard('admin')->check()) {
        return redirect(route('admin.dashboard'));
      }

      if (Auth::check()) {
        if (Auth::user()->type == 'teacher') {
          return redirect(route('teacher.feeds'));
        } elseif(Auth::user()->type == 'student') {
          return redirect(route('student.feeds'));
        }
      }

        return $next($request);
    }
}
