<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Profile;
use Auth;


class ProfileController extends Controller
{
  public function index(User $user)
  {

    $users = $user;
    $subjects = $user->subjects->all();

    return view('teacher.profile',compact('users', 'subjects'));
  }


    public function saveProfileInformation(Request $request)
    {
      if (Auth::user()->id != $request->auid) {
        return redirect()->back();
      }



        if($request->file('profile_pic')) {
          $fileId = null;
          $request->file('profile_pic')->move(public_path() . '/uploads/profile-pic', $fileId = uniqid(true) . '.png');
          User::where('id', $request->auid)->update([
            'profile_pic' => $fileId
          ]);
        }


      Profile::where('user_id', $request->auid)->update([
        'about_me' => $request->about_me,
        'education' => $request->education
      ]);

      return redirect()->back();
    }
}
