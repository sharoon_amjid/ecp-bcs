<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Semester;
use App\Batch;
use App\User;
use DB;

class SemesterController extends Controller
{
    public function index()
    {
      return view('teacher.semesters');
    }

    public function semesterList($user_id)
    {
      $type = 'student';
      return DB::select("SELECT DISTINCT
         semesters.display_name as semester_name, semesters.id as semester_id,
         batches.display_name as batch_name, batches.id as batches_id,
         (SELECT COUNT(id) FROM users WHERE batch_id = batches.id AND type = '$type') as total_student,
         (SELECT COUNT(id) FROM subjects WHERE user_id = users.id AND semester_id = semesters.id) as total_subjects
         FROM batches INNER JOIN semesters
         ON batches.id = semesters.batch_id INNER JOIN subjects
         ON semesters.id = subjects.semester_id INNER JOIN users
         ON subjects.user_id = users.id WHERE users.id = ?", [$user_id]);
    }
}
