<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Feed;
use App\User;


class FeedController extends Controller
{


    public function index()
    {
      return view('teacher.feeds');
    }

    public function getFeeds()
    {
      return Feed::all();
    }

    public function store(Request $request, User $user)
    {
      $fileId = null;
      if($request->file('image_filename')) {
        $request->file('image_filename')->move(public_path() . '/uploads', $fileId = uniqid(true) . '.png');
      }

        $user->feeds()->create([
        'image_filename' => $fileId,
        'description' => $request->description
      ]);

      return redirect()->back();
    }
}
