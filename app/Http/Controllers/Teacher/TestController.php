<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Test;
use App\TestDetail;
use DB;
class TestController extends Controller
{
    public function testDetails(Request $request)
    {
      return DB::select('SELECT tests.total_marks, tests.passing_marks, users.name, users.college_id, test_details.obtained_marks
                        FROM test_details
                        INNER JOIN tests ON test_details.test_id = tests.id
                        INNER JOIN users ON test_details.user_id = users.id
                        WHERE test_details.test_id = ?', [$request->test_id]);
    }
    public function testList(Request $request)
    {
      return Test::where('semester_id', $request->semester_id)->where('batch_id', $request->batch_id)->where('subject_id', $request->subject_id)->get();
    }

    public function addMarks(Request $request)
    {
      $user_id = $request->user_id;
      $batch_id = $request->batch_id;
      $semester_id = $request->semester_id;
      $subject_id = $request->subject_id;
      $total_marks = $request->total_marks;
      $minimum_marks = $request->minimum_marks;
      $test_type = $request->test_type;

      $test = Test::create([
        'test_type' => $test_type,
        'total_marks' => $total_marks,
        'passing_marks' => $minimum_marks,
        'user_id' => $user_id,
        'subject_id' => $subject_id,
        'batch_id' => $batch_id,
        'semester_id' => $semester_id
      ]);

      foreach ($request->student_ids as $student_details) {
        $obtained_marks = $student_details['marks'];
        $student_id = $student_details['id'];

        TestDetail::create([
          'user_id' => $student_id,
          'test_id' => $test->id,
          'obtained_marks' => $obtained_marks
        ]);
      };

      return response(null, 200);
    }
}
