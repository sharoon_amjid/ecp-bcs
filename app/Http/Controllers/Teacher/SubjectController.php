<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SubjectController extends Controller
{
    public function subjectList(Request $request)
    {
        return DB::select("SELECT subjects.display_name AS subjects_name, subjects.id AS subjects_id,  semesters.id AS semesters_id, batches.id AS batches_id
                            FROM batches
                            INNER JOIN semesters
                            ON batches.id = semesters.batch_id
                            INNER JOIN subjects
                            ON semesters.id = subjects.semester_id
                            INNER JOIN users
                            ON subjects.user_id = users.id
                            WHERE users.id = ?
                            AND subjects.semester_id=?", [$request->user_id, $request->semester_id]);
    }

}
