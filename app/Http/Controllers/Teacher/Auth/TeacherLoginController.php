<?php

namespace App\Http\Controllers\Teacher\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class TeacherLoginController extends Controller
{

    public function __construct()
    {
      $this->middleware('guest:teacher');
    }
    public function showLoginForm()
    {
      return view('teacher.auth.teacher-login');
    }

    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email' => 'required|email',
        'password' => 'required|min:6'
      ]);

      // Attempt to log the user in
      if (Auth::guard('teacher')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // If successful, then redirect to their intended location
        return redirect()->intended(route('teacher.feeds'));
      }

      return redirect()->back()->withInput($request->only('email'));
    }
}
