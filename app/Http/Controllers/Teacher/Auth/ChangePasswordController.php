<?php

namespace App\Http\Controllers\Teacher\Auth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class ChangePasswordController extends Controller
{
    public function changePassword(Request $request)
    {
          $user = Auth::user();

          $messages = [
            'currentpassword' => 'Current password is incorrect.',
            'required' => 'Please fill this field.',
            'min' => 'It must be atleast 6 characters.'

          ];
          $validator = Validator::make($request->all(), [
            'currentpassword' => 'required|min:6|currentpassword:'.$user->password,
            'newpassword' => 'required|min:6|confirmed',
          ], $messages)->validate();


          $user->update([
              'password' => bcrypt($request->newpassword),
          ]);

          return redirect(route('teacher.profile'))->with(['status' => 'Your Password is Updated successfully.', 'type' => 'success', 'icon' => 'pe-7s-like2']);


    }
}
