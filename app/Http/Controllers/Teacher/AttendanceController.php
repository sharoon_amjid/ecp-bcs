<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Attendance;
use App\AttendanceDetail;
use DB;

class AttendanceController extends Controller
{
    public function getAttendanceStatus(Request $request)
    {
      $subject_id = $request->subject_id;
      $batch_id = $request->batch_id;
      return DB::select("SELECT  users.id as user_id, users.college_id, users.name, users.gender, users.profile_pic, (SELECT count(attendances.id)
                  FROM attendances
                  WHERE attendances.subject_id = ? AND attendances.batch_id = users.batch_id) AS total,
                  (SELECT count(attendance_details.id) FROM attendance_details INNER JOIN attendances ON attendance_details.attendance_id = attendances.id
                  WHERE attendance_details.user_id = users.id AND status = 'present' and attendances.subject_id = ?) AS obtained,
                  ROUND((((SELECT count(attendance_details.id) FROM attendance_details INNER JOIN attendances ON attendance_details.attendance_id = attendances.id WHERE attendance_details.user_id = users.id AND status = 'present' AND attendances.subject_id = ?) * 100) / (SELECT count(attendances.id) FROM attendances WHERE attendances.subject_id = ? AND attendances.batch_id = users.batch_id)),0) AS percentage,
                  ROUND((((SELECT count(attendance_details.id) FROM attendance_details INNER JOIN attendances ON attendance_details.attendance_id = attendances.id WHERE attendance_details.user_id = users.id and Status = 'present' and attendances.subject_id = ?) * 100) / (SELECT count(attendances.id) from attendances WHERE attendances.subject_id = ? and attendances.batch_id = users.batch_id))/20, 1) as rating FROM users
                  WHERE users.batch_id = ?", [$subject_id,$subject_id,$subject_id,$subject_id,$subject_id,$subject_id,$batch_id]);
    }

    public function updatetLastAttendance(Request $request)
    {
      AttendanceDetail::where('id', $request->attendance_detail_id)
                      ->where('user_id',$request->user_id)->update([
        'status' => $request->status
      ]);

      return response(null, 200);
    }

    public function getLastAttendance(Request $request)
    {
      $subject_id = $request->subject_id;
      $lastAttendanceId = Attendance::where('subject_id', $subject_id)->max('id');

      return DB::table('users')
                ->join('attendance_details', 'users.id', '=', 'attendance_details.user_id')
                ->where('attendance_details.attendance_id','=', $lastAttendanceId)
                ->select('users.name', 'users.id as user_id', 'users.college_id', 'attendance_details.attendance_id as attendance_id', 'attendance_details.id as attendance_detail_id', 'attendance_details.status')
                ->get();
    }

    public function getStudentForAttendance(Request $request)
    {
      $type = 'student';
      return User::where('batch_id',$request->batch_id)
            ->where('type',$type)->get();
    }


    public function addNewAttendance(Request $request)
    {
      $type = 'student';

      if(!empty($request->student_ids))
      {
        $attendance = Attendance::Create([
          'user_id' => $request->user_id,
          'batch_id' => $request->batch_id,
          'subject_id' => $request->subject_id,
          'semester_id' => $request->semester_id,
        ]);

        $attendance_id = $attendance->id;

        $presentStudents = $request->student_ids;
        $absentStudents = User::whereNotIn('id',$request->student_ids)->where('batch_id', $request->batch_id)->where('type', $type)->pluck('id');
        foreach ($presentStudents as $presentStudent) {
          AttendanceDetail::Create([
            'user_id' => $presentStudent,
            'attendance_id' => $attendance_id,
            'status' => 'present',
          ]);
        }

        foreach ($absentStudents as $absentStudent) {
          AttendanceDetail::Create([
            'user_id' => $absentStudent,
            'attendance_id' => $attendance_id,
            'status' => 'absent',
          ]);
        }

        return response(null, 200);
      }
      else
      {
        $attendance = Attendance::Create([
          'user_id' => $request->user_id,
          'batch_id' => $request->batch_id,
          'subject_id' => $request->subject_id,
          'semester_id' => $request->semester_id,
        ]);

        $attendance_id = $attendance->id;

        $absentStudents = User::where('batch_id', $request->batch_id)->where('type', $type)->pluck('id');
        foreach ($absentStudents as $absentStudent) {
          AttendanceDetail::Create([
            'user_id' => $absentStudent,
            'attendance_id' => $attendance_id,
            'status' => 'absent',
          ]);
        }
        return response(null, 200);
      }
    }
}
