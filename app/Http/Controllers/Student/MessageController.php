<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Messaging;

class MessageController extends Controller
{
    public function index()
    {
      return view('student.messages');
    }

    public function storeMessaging(Request $request)
    {

      $messaging_id = null;
      $messaging_id = DB::select('SELECT id FROM messagings
      WHERE (user_one = ? AND user_two = ?) OR (user_one = ? AND user_two = ?)', [$request->user_one, $request->user_two, $request->user_two, $request->user_one]);

      if(count($messaging_id) == 0)
      {
        Messaging::Create([
          'user_one' => $request->user_one,
          'user_two' => $request->user_two,
          'ip_address' => $request->ip(),
        ]);
      }
      return response(null, 200);
    }
    public function getUserForMessage(Request $request, $username)
    {
      return User::where('name', 'LIKE', '%'.$username.'%')
                  ->where('id', '<>', $request->self)
                  ->orWhere('college_id', 'LIKE', '%'.$username.'%')
                  ->limit(4)
                  ->get();
    }

    public function conversationList(Request $request)
    {
      $user_id = $request->user()->id;

      $conversationList = DB::select('SELECT m.id AS messaging_id, u.id AS user_id,u.name,u.profile_pic, u.type, m.updated_at
      FROM messagings m, users u
      WHERE CASE WHEN m.user_one = ? THEN m.user_two = u.id WHEN m.user_two = ? THEN m.user_one= u.id END
      AND (m.user_one = ? OR m.user_two = ?) ORDER BY m.id DESC', [$user_id, $user_id, $user_id, $user_id]);

/*
      ALTERNATIVE SOLUTION

      $conversationList = DB::select(DB::raw("SELECT u.id,m.id,u.name,u.profile_pic
                          FROM messagings m, users u
                          WHERE
                            CASE
                            WHEN m.user_one = $user_id THEN m.user_two = u.id
                            WHEN m.user_two = $user_id THEN m.user_one= u.id
                            END
                            AND
                            (m.user_one = $user_id OR m.user_two = $user_id)
                            ORDER BY m.id DESC"
                          ));

*/
      return $conversationList;
    }

    public function conversationReply($message_id)
    {

        $conversationReply = DB::select('SELECT mr.id AS message_reply_id, mr.created_at, mr.reply, u.id AS user_id, u.name, u.profile_pic
        FROM users u, messaging_replies mr WHERE mr.user_id_fk=u.id
        AND mr.messaging_id_fk=? ORDER BY mr.id ASC', [$message_id]);

        return $conversationReply;
    }

    public function storeConversation(Request $request)
    {

        $messaging_id = null;
        $messaging_id = DB::select('SELECT id FROM messagings
        WHERE (user_one = ? AND user_two = ?) OR (user_one = ? AND user_two = ?)', [$request->user_one, $request->user_two, $request->user_two, $request->user_one]);

        if(count($messaging_id) == 0)
        {
          Messaging::Create([
            'user_one' => $request->user_one,
            'user_two' => $request->user_two,
            'ip_address' => $request->ip(),
          ]);

          $messaging_id = DB::select('SELECT id FROM messagings WHERE user_one=? ORDER BY id DESC limit 1', [$request->user_one]);

        }
        $messaging_id = $messaging_id[0]->id;
        DB::insert('INSERT INTO messaging_replies (user_id_fk,reply,image_filename,ip_address,messaging_id_fk) VALUES (?,?,?,?,?)',
                    [$request->user_one, $request->reply, $request->image_filename, $request->ip(), $messaging_id]);

        return response(null,200);
    }
}
