<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Feed;
use App\Comment;

class FeedCommentController extends Controller
{

  public function index(Feed $feed)
  {

    return response()->json($feed->latest()->with(['user', 'comments' => function($query) {
      $query->with(['user', 'replies' => function($query) {
        $query->with(['user']);
      }]);
    }])->get(), 200);

  }

  public function profileActivityFeeds(Request $request, Feed $feed)
  {
    return response()->json($feed->where('user_id', $request->user_id)->latest()->with(['user', 'comments' => function($query) {
      $query->with(['user', 'replies' => function($query) {
        $query->with(['user']);
      }]);
    }])->get(), 200);
  }

  public function store(Request $request, Feed $feed)
  {
    $validator = Validator::make($request->all(), [
      'body' => 'required',
      'reply_id' => 'exists:comments,id',
      'user_id' => 'required',
    ]);

    if ($validator->fails())
    {
      return  $message = response($validator->errors(),500);
    }

    $comment = $feed->comments()->create([
      'body' => $request->body,
      'reply_id' => $request->get('reply_id', null),
      'user_id' => $request->user_id,
    ]);
  }

}
