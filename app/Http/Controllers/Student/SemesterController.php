<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Semester;
use App\Batch;
use App\User;
use DB;

class SemesterController extends Controller
{
    public function index()
    {
      return view('student.semesters');
    }

    public function semesterList($user_id)
    {
      $type = 'student';
      return DB::select("SELECT DISTINCT semesters.id as semester_id,
                        semesters.display_name as semester_name,
                        batches.id as b_id_from_batch,
                        batches.display_name as batch_name,
                        users.batch_id as batch_id,
                        (SELECT COUNT(users.id) FROM users WHERE users.batch_id = batches.id) as total_student,
                        (SELECT COUNT(id) FROM subjects WHERE semester_id = semesters.id) as total_subjects
                        FROM attendances INNER JOIN attendance_details
                        ON attendances.id = attendance_details.attendance_id INNER JOIN semesters
                        ON attendances.semester_id = semesters.id INNER JOIN batches
                        ON attendances.batch_id = batches.id INNER JOIN users
                        ON attendances.batch_id = users.batch_id WHERE attendance_details.user_id = ?", [$user_id]);
    }
}
