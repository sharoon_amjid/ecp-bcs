<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SubjectController extends Controller
{
    public function subjectList(Request $request)
    {
        return DB::select("SELECT DISTINCT subjects.id as subject_id,
                          subjects.display_name as subject_name,
                          attendances.batch_id as batch_id,
                          subjects.user_id as teacher_id,
                          users.name, users.profile_pic
                          FROM attendances
                          INNER JOIN subjects
                          ON attendances.subject_id = subjects.id
                          INNER JOIN users
                          ON subjects.user_id = users.id
                          WHERE subjects.semester_id = ?", [$request->semester_id]);
    }

}
