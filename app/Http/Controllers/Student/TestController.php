<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Test;

class TestController extends Controller
{
  public function testDetails(Request $request)
  {
    return DB::select('SELECT tests.total_marks, tests.passing_marks, users.name, users.college_id, test_details.obtained_marks
                      FROM test_details
                      INNER JOIN tests ON test_details.test_id = tests.id
                      INNER JOIN users ON test_details.user_id = users.id
                      WHERE test_details.test_id = ?', [$request->test_id]);
  }
  public function testList(Request $request)
  {
    return Test::where('semester_id', $request->semester_id)->where('batch_id', $request->batch_id)->where('subject_id', $request->subject_id)->get();
  }
}
