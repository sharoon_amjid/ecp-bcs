<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use App\User;

class SubjectController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth:admin');
  }

    public function show()
    {
        return view('admin.actions.manage-subject');
    }

    public function subjectList()
    {
      return $subjects = Subject::with(['user' => function($query) {
                        $query->where('type','teacher');
      }])->get();
    }

    public function UnassignSubjectFromTeacher(Request $request, Subject $subject)
    {
        $subject->update([
            'user_id' => null,
        ]);

        return response(null, 200);
    }

    public function assignSubjectToTeacher(Request $request, Subject $subject)
    {
        $subject->update([
          'user_id' => $request->user_id,
        ]);

        return response(null, 200);
    }
}
