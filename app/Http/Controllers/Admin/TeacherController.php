<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function teacherWithSubject()
    {
      return User::where('type','teacher')->with('subjects')->get();
    }

    public function show()
    {
      return User::where('type','teacher')->get();
    }


}
