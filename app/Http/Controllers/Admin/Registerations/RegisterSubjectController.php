<?php

namespace App\Http\Controllers\Admin\Registerations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;

class RegisterSubjectController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth:admin');
  }

  public function store()
  {

    if (Subject::count() < 1) {

      Subject::registerSubjects();

      return redirect()->back()->with(['status' => 'Subjects are added successfully, Select teachers for their subjects', 'type' => 'success', 'icon' => 'pe-7s-diskette']);

    } else {

      return redirect()->back()->with(['status' => 'Subjects are available already', 'type' => 'warning', 'icon' => 'pe-7s-attention']);

    }

  }
}
