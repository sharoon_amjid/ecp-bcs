<?php

namespace App\Http\Controllers\Admin\Registerations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Semester;

class RegisterSemesterController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth:admin');
  }

  public function store()
  {

    if (Semester::count() < 1) {

      Semester::registerSemesters();

      return redirect()->back()->with(['status' => 'Semesters are added successfully', 'type' => 'success', 'icon' => 'pe-7s-diskette']);

    } else {

      return redirect()->back()->with(['status' => 'Semesters are available already', 'type' => 'warning', 'icon' => 'pe-7s-attention']);

    }

  }
}
