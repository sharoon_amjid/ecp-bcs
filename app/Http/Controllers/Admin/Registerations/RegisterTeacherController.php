<?php

namespace App\Http\Controllers\Admin\Registerations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;

class RegisterTeacherController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  public function show()
  {
      return view('admin.registerations.teacher-register');
  }

  public function store(Request $request)
  {

    $validator = Validator::make($request->all(), [
      'name' => 'required|regex:/^[\pL\s]+$/u',
      'college_id' => 'required|numeric|unique:users,college_id',
      'gender' => 'required|in:male,female',
      'email' => 'required|email|unique:users,email',
      'title' => 'required',
      'password' => 'required|min:6|confirmed',
    ]);

    if ($validator->fails())
    {
      return redirect(route('register.teacher'))->withErrors($validator)->withInput();
    }
    $user = User::create([
        'name' => $request->name,
        'college_id' => $request->college_id,
        'gender' => $request->gender,
        'profile_pic' => '2344525.png',
        'api_token' => str_random(60),
        'email' => $request->email,
        'title' => $request->title,
        'password' => bcrypt($request->password),
        'type' => 'teacher',
    ]);

    $user->profile()->create([
        'about_me' => null,
        'education' => null,
    ]);

    return redirect(route('register.teacher'))->with(['status' => $request->name . ' is registered successfully, Email is send with all credentials.', 'type' => 'success', 'icon' => 'pe-7s-like2']);



  }

}
