<?php

namespace App\Http\Controllers\Admin\Registerations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Batch;

class RegisterStudentController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  public function show()
  {
      $batches = Batch::all();
      return view('admin.registerations.student-register', compact('batches'));
  }

  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name' => 'required|regex:/^[\pL\s]+$/u',
      'college_id' => 'required|numeric|unique:users,college_id',
      'gender' => 'required|in:male,female',
      'email' => 'required|email|unique:users,email',
      'password' => 'required|min:6|confirmed',
      'batch' => 'required',
    ]);

    if ($validator->fails())
    {
      return redirect(route('register.student'))->withErrors($validator)->withInput();
    }

    $user = User::create([
        'name' => $request->name,
        'college_id' => $request->college_id,
        'gender' => $request->gender,
        'profile_pic' => '2344525.png',
        'email' => $request->email,
        'api_token' => str_random(60),
        'password' => bcrypt($request->password),
        'batch_id' => $request->batch,
        'type' => 'student',
    ]);

    $user->profile()->create([
        'about_me' => null,
        'education' => null,
    ]);


    return redirect(route('register.student'))->with(['status' => $request->name . ' is registered successfully, Email is send with all credentials.', 'type' => 'success', 'icon' => 'pe-7s-like2']);

  }

}
