<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Semester;
use App\Batch;

class BatchController extends Controller
{
  
  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  public function registerBatch(Request $request, Semester $semester)
  {

    $validator = Validator::make($request->all(), [
      'batch_name' => 'required|regex:/^[a-zA-Z0-9-]+$/',
    ]);

    if ($validator->fails())
    {
      return  $message = response($validator->errors(),500);
    }

    $batch = Batch::create([
      'display_name' => $request->batch_name,
    ]);

    $semester = $semester->update([
      'batch_id' => $batch->id,
    ]);



    if($batch == true && $semester == true)
    {
      return response(null, 200);
    }

    return response(null, 500);

  }
}
