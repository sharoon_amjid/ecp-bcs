<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Semester;
use App\Batch;
use App\User;
use DB;

class SemesterController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  public function assignBatchToSemester(Request $request, Semester $semester)
  {

    $validator = Validator::make($request->all(), [
      'batch_id' => 'required|numeric',
    ]);

    if ($validator->fails())
    {
      return  $message = response($validator->errors(),500);
    }

    $update = $semester->update([
      'batch_id' => $request->batch_id
    ]);

    if($update)
    {
      $batch = Batch::where('id', $request->batch_id)->update(['studying' => '1']);

      if($batch)
      {
        return response(null, 200);
      }
      return response(null, 500);
    }
    return response(null, 500);
  }

  public function manageSemester()
  {
    return view('admin.actions.manage-semester');
  }

  public function semesterList()
  {
    return Semester::all();
  }

  public function freeSemester()
  {
    return Semester::where('batch_id',null)->get();
  }

  public function freeBatch()
  {
    return Batch::where('studying','0')->where('active','1')->get();
  }

  public function batchComplete(Request $request, Semester $semester)
  {

    $validator = Validator::make($request->all(), [
      'batch_id' => 'required|numeric',
    ]);

    if ($validator->fails())
    {
      return  $message = response($validator->errors(),500);
    }

    $update = $semester->update([
      'batch_id' => $request->batch_id
    ]);
    $updateBatch = Batch::where('id', $request->batch_id)->update(['studying' => '0']);
    $updateSemester = Semester::where('batch_id', $request->batch_id)->update(['batch_id' => null]);

    if($updateBatch && $updateSemester)
    {
      return response(null, 200);
    }
    return response(null, 500);
  }

  public function batchLeave(Request $request, Semester $semester)
  {

    $validator = Validator::make($request->all(), [
      'batch_id' => 'required|numeric',
    ]);

    if ($validator->fails())
    {
      return  $message = response($validator->errors(),500);
    }

    $update = $semester->update([
      'batch_id' => $request->batch_id
    ]);

    $updateSemester = $semester->update(['batch_id' => null]);
    $updateBatch = Batch::where('id',$request->batch_id)->update(['studying' => '0', 'active' => '0']);

    if($updateSemester && $updateBatch)
    {
      return response(null, 200);
    }

    return response(null, 500);
  }

  public function semesterDetail()
  {
    return Batch::where('active','1')->where('studying','1')->with(['semesters', 'users' => function($query){
      $query->where('type','student');
    }])->get();
  }

  public function batchDetails()
  {
    return Batch::where('batch', !null);
  }

}
