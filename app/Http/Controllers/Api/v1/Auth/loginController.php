<?php

namespace App\Http\Controllers\Api\v1\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class loginController extends Controller
{
    public function login(Request $request)
    {

      $response = [
          'error' => true,
          'error_msg' => null,
      ];

      if (!empty($request->email) && !empty($request->password)) {
        //$hashed_password = hash::make($request->password);
        $user = User::where('email', $request->email)->first();


        if (count($user) > 0) {
          $hashedPassword = $user->password;
          if (\Hash::check($request->password, $hashedPassword)) {
            $response['error'] = false;
            $response['data'] = $user;
            //return $user;
            return response()->json([
               'response' => $response
            ], 200);
          } else {
            $response['error'] = true;
            $response['error_msg'] = "Login credentials are wrong";

            return response()->json(['response' => $response],200);
          }
        } else {
          $response['error'] = true;
          $response['error_msg'] = "Login credentials are wrong";

          return response()->json(['response' => $response],200);
        }


        if ($user != false) {
          $response['error'] = false;
          $response['data'] = $user;
          //return $user;
          return response()->json([
             'response' => $response
          ], 200);

        }
        else
        {
          $response['error'] = true;
          $response['error_msg'] = "Login credentials are wrong";

          return response()->json(['response' => $response],200);
        }
      } else {
        $response['error'] = true;
        $response['error_msg'] = "Please Enter complete credentials";

        return response()->json(['response' => $response],200);
      }


    }
}
