<?php

namespace App\Http\Controllers\Api\v1\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class SemesterController extends Controller
{
  public function semesterList(Request $request)
  {
      $response = [
          'error' => true,
          'error_msg' => null,
      ];
    
      $type = 'student';
      $semester_list = DB::select("SELECT DISTINCT
       semesters.display_name as semester_name, semesters.id as semester_id,
       batches.display_name as batch_name, batches.id as batches_id,
       (SELECT COUNT(id) FROM users WHERE batch_id = batches.id AND type = '$type') as total_student,
       (SELECT COUNT(id) FROM subjects WHERE user_id = users.id AND semester_id = semesters.id) as total_subjects
       FROM batches INNER JOIN semesters
       ON batches.id = semesters.batch_id INNER JOIN subjects
       ON semesters.id = subjects.semester_id INNER JOIN users
       ON subjects.user_id = users.id WHERE users.id = ?", [$request->user_id]);

       if ($semester_list != false) {
         $response['error'] = false;
         $response['data'] = $semester_list;

         return response()->json([
            'response' => $response
         ], 200);

       }
       else
       {
         $response['error'] = true;
         $response['error_msg'] = "No data found";

         return response()->json(['response' => $response],200);
       }
  }
}
