<?php

namespace App\Http\Controllers\Api\v1\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Feed;

class FeedCommentController extends Controller
{



    public function index(Feed $feed)
    {
      return response()->json($feed->latest()->with(['user', 'comments' => function($query) {
        $query->with(['user', 'replies' => function($query) {
          $query->with(['user']);
        }]);
      }])->get(), 200);
    }

    public function getSingleFeed(Feed $feed, Request $request)
    {
      return response()->json($feed->where('id', $request->feed_id)->latest()->with(['user', 'comments' => function($query) {
        $query->with(['user', 'replies' => function($query) {
          $query->with(['user']);
        }]);
      }])->get(), 200);
    }


}
