<?php

namespace App\Http\Controllers\Api\v1\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attendance;
use App\AttendanceDetail;
use App\User;
use DB;

class AttendanceController extends Controller
{

  public function addNewAttendance(Request $request)
  {
    $type = 'student';
    $student_ids = (array) json_decode($request->student_ids);
    if (!empty($request->user_id) && !empty($request->batch_id) && !empty($request->semester_id) && !empty($request->subject_id)) {
      if(!empty($request->student_ids))
      {

        $attendance = Attendance::Create([
          'user_id' => $request->user_id,
          'batch_id' => $request->batch_id,
          'subject_id' => $request->subject_id,
          'semester_id' => $request->semester_id,
        ]);

        $attendance_id = $attendance->id;

        $presentStudents = $student_ids;

        $absentStudents = User::whereNotIn('id', $presentStudents)->where('batch_id', $request->batch_id)->where('type', $type)->pluck('id');
        foreach ($presentStudents as $presentStudent) {
          AttendanceDetail::Create([
            'user_id' => $presentStudent,
            'attendance_id' => $attendance_id,
            'status' => 'present',
          ]);
        }

        foreach ($absentStudents as $absentStudent) {
          AttendanceDetail::Create([
            'user_id' => $absentStudent,
            'attendance_id' => $attendance_id,
            'status' => 'absent',
          ]);
        }

        return response("Saved", 200);
      }
      else
      {
        $attendance = Attendance::Create([
          'user_id' => $request->user_id,
          'batch_id' => $request->batch_id,
          'subject_id' => $request->subject_id,
          'semester_id' => $request->semester_id,
        ]);

        $attendance_id = $attendance->id;

        $absentStudents = User::where('batch_id', $request->batch_id)->where('type', $type)->pluck('id');
        foreach ($absentStudents as $absentStudent) {
          AttendanceDetail::Create([
            'user_id' => $absentStudent,
            'attendance_id' => $attendance_id,
            'status' => 'absent',
          ]);
        }
        return response("Saved", 200);
      }
    } else {
      $response['error'] = true;
      $response['error_msg'] = "Required Fields are empty";

      return response()->json(['response' => $response],200);
    }

  }

  public function updateLastAttendance(Request $request)
  {
    $update_attendance = AttendanceDetail::where('id', $request->attendance_detail_id)
                    ->where('user_id',$request->user_id)->update([
      'status' => $request->status
    ]);

    $response = [
        'error' => true,
        'error_msg' => null,
    ];

    if ($update_attendance != false) {
      $response['error'] = false;
      $response['data'] = $this->getLastUpdatedAttendance($request->attendance_detail_id);

      return response()->json([
         'response' => $response
      ], 200);

    }
    else
    {
      $response['error'] = true;
      $response['error_msg'] = "No data found";

      return response()->json(['response' => $response],200);
    }
  }


  public function getLastUpdatedAttendance($attendance_detail_id)
  {
    return DB::table('users')
              ->join('attendance_details', 'users.id', '=', 'attendance_details.user_id')
              ->where('attendance_details.id','=', $attendance_detail_id)
              ->select('users.name', 'users.id as user_id', 'users.college_id', 'attendance_details.attendance_id as attendance_id', 'attendance_details.id as attendance_detail_id', 'attendance_details.status')
              ->get();
  }



  public function getAttendanceStatus(Request $request)
  {
    $subject_id = $request->subject_id;
    $batch_id = $request->batch_id;
    $attendance_status = DB::select("SELECT  users.id as user_id, users.college_id, users.name, users.gender, users.profile_pic, (SELECT count(attendances.id)
                FROM attendances
                WHERE attendances.subject_id = ? AND attendances.batch_id = users.batch_id) AS total,
                (SELECT count(attendance_details.id) FROM attendance_details INNER JOIN attendances ON attendance_details.attendance_id = attendances.id
                WHERE attendance_details.user_id = users.id AND status = 'present' and attendances.subject_id = ?) AS obtained,
                ROUND((((SELECT count(attendance_details.id) FROM attendance_details INNER JOIN attendances ON attendance_details.attendance_id = attendances.id WHERE attendance_details.user_id = users.id AND status = 'present' AND attendances.subject_id = ?) * 100) / (SELECT count(attendances.id) FROM attendances WHERE attendances.subject_id = ? AND attendances.batch_id = users.batch_id)),0) AS percentage,
                ROUND((((SELECT count(attendance_details.id) FROM attendance_details INNER JOIN attendances ON attendance_details.attendance_id = attendances.id WHERE attendance_details.user_id = users.id and Status = 'present' and attendances.subject_id = ?) * 100) / (SELECT count(attendances.id) from attendances WHERE attendances.subject_id = ? and attendances.batch_id = users.batch_id))/20, 1) as rating FROM users
                WHERE users.batch_id = ?", [$subject_id,$subject_id,$subject_id,$subject_id,$subject_id,$subject_id,$batch_id]);

    $response = [
        'error' => true,
        'error_msg' => null,
    ];

    if ($attendance_status != false) {
      $response['error'] = false;
      $response['data'] = $attendance_status;

      return response()->json([
         'response' => $response
      ], 200);

    }
    else
    {
      $response['error'] = true;
      $response['error_msg'] = "No data found";

      return response()->json(['response' => $response],200);
    }
  }


  public function getStudentForAttendance(Request $request)
  {
    $type = 'student';
    $studentList =  User::where('batch_id',$request->batch_id)
          ->where('type',$type)->get();

          $response = [
              'error' => true,
              'error_msg' => null,
          ];

          if ($studentList != false) {
              $response['error'] = false;
              $response['data'] = $studentList;

              return response()->json([
                 'response' => $response
              ], 200);
          }
          else
          {
            $response['error'] = true;
            $response['error_msg'] = "No data found";

            return response()->json(['response' => $response],200);
          }
  }

  public function getLastAttendance(Request $request)
  {
    $subject_id = $request->subject_id;
    $lastAttendanceId = Attendance::where('subject_id', $subject_id)->max('id');

    $studentList =  DB::table('users')
                    ->join('attendance_details', 'users.id', '=', 'attendance_details.user_id')
                    ->where('attendance_details.attendance_id','=', $lastAttendanceId)
                    ->select('users.name', 'users.id as user_id', 'users.college_id', 'attendance_details.attendance_id as attendance_id', 'attendance_details.id as attendance_detail_id', 'attendance_details.status')
                    ->get();

    $response = [
        'error' => true,
        'error_msg' => null,
    ];

    if ($studentList != false) {
        $response['error'] = false;
        $response['data'] = $studentList;

        return response()->json([
           'response' => $response
        ], 200);
    }
    else
    {
      $response['error'] = true;
      $response['error_msg'] = "No data found";

      return response()->json(['response' => $response],200);
    }
  }
}
