<?php

namespace App\Http\Controllers\Api\v1\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SubjectController extends Controller
{
  public function subjectList(Request $request)
  {
      $response = [
          'error' => true,
          'error_msg' => null,
      ];

      $subject_list = DB::select("SELECT subjects.display_name AS subjects_name, subjects.id AS subjects_id,
                          semesters.id AS semesters_id, batches.id AS batches_id
                          FROM batches
                          INNER JOIN semesters
                          ON batches.id = semesters.batch_id
                          INNER JOIN subjects
                          ON semesters.id = subjects.semester_id
                          INNER JOIN users
                          ON subjects.user_id = users.id
                          WHERE users.id = ?
                          AND subjects.semester_id=?", [$request->user_id, $request->semester_id]);

      if ($subject_list != false) {
        $response['error'] = false;
        $response['data'] = $subject_list;

        return response()->json([
           'response' => $response
        ], 200);

      }
      else
      {
        $response['error'] = true;
        $response['error_msg'] = "No data found";

        return response()->json(['response' => $response],200);
      }


  }
}
