<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Auth;

class StudentNavigationComposer
{

  public function compose(View $view)
  {
    if(!Auth::check())
    {
      return;
    }

    $view->with('user', Auth::user());
  }
}
