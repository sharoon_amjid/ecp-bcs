<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','api_token', 'gender', 'profile_pic', 'college_id','title', 'batch_id', 'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
      return $this->hasOne('App\Profile');
    }

    public function feeds()
    {
      return $this->hasMany('App\Feed');
    }

    public function batch()
    {
      return $this->belongsTo('App\Batch');
    }

    public function subjects()
    {
      return $this->hasMany('App\Subject');
    }

    public function attendanceDetail()
    {
      return $this->belongsToMany('App\AttendanceDetail');
    }


    public function getRouteKeyName()
    {
      return 'college_id';
    }
}
