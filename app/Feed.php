<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
  protected $fillable = [
      'user_id', 'image_filename', 'audio_filename', 'video_filename', 'description',
  ];

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function comments()
  {
    return $this->morphMany(Comment::class, 'commentable')->whereNull('reply_id');
  }

}
