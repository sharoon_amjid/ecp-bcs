<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messaging extends Model
{
  protected $fillable = [
      'user_one', 'user_two', 'ip_address',
  ];
}
