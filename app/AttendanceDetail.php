<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceDetail extends Model
{
  protected $fillable = [
      'user_id', 'attendance_id', 'status',
  ];

  public function users()
  {
    return $this->hasMany(User::class);
  }
}
