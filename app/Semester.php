<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'slug', 'display_name', 'batch_id',
  ];

  public function batches()
  {
    return $this->belongsToMany('App\Batch');
  }

  public function subjects()
  {
    return $this->HasMany('App\Subject');
  }

  public function getRouteKeyName()
  {
    return 'slug';
  }

  public static function registerSemesters()
  {
    Semester::Create([
      'slug' => 'semester-one',
      'display_name' => 'Semester One',
    ]);

    Semester::Create([
      'slug' => 'semester-two',
      'display_name' => 'Semester Two',
    ]);

    Semester::Create([
      'slug' => 'semester-three',
      'display_name' => 'Semester Three',
    ]);

    Semester::Create([
      'slug' => 'semester-four',
      'display_name' => 'Semester Four',
    ]);

    Semester::Create([
      'slug' => 'semester-five',
      'display_name' => 'Semester Five',
    ]);

    Semester::Create([
      'slug' => 'semester-six',
      'display_name' => 'Semester Six',
    ]);

    Semester::Create([
      'slug' => 'semester-seven',
      'display_name' => 'Semester Seven',
    ]);

    Semester::Create([
      'slug' => 'semester-eight',
      'display_name' => 'Semester Eight',
    ]);
  }

}
