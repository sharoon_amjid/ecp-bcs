<div class="sidebar" data-color="azure" data-image="#">

<!--

    Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
    Tip 2: you can also add an image using data-image tag

-->

  <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                Creative Tim
            </a>
        </div>

        <ul class="nav">
            <li class="">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="pe-7s-graph"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="">
                <a href="{{ route('register.teacher') }}">
                    <i class="pe-7s-user"></i>
                    <p>Register Teacher</p>
                </a>
            </li>
            <li>
                <a href="{{ route('manage.subject') }}">
                    <i class="pe-7s-note2"></i>
                    <p>Manage Subjects</p>
                </a>
            </li>
            <li>
                <a href="{{route('manage.semester')}}">
                    <i class="pe-7s-news-paper"></i>
                    <p>Manage Semesters</p>
                </a>
            </li>
            <li>
                <a href="{{ route('register.student')}}">
                    <i class="pe-7s-science"></i>
                    <p>Register Student</p>
                </a>
            </li>
            <li>
                <a href="">
                    <i class="pe-7s-map-marker"></i>
                    <p>Maps</p>
                </a>
            </li>
            <li>
                <a href="notifications.html">
                    <i class="pe-7s-bell"></i>
                    <p>Notifications</p>
                </a>
            </li>
    <li class="active-pro">
                <a href="upgrade.html">
                    <i class="pe-7s-rocket"></i>
                    <p>Upgrade to PRO</p>
                </a>
            </li>
        </ul>
  </div>
</div>
