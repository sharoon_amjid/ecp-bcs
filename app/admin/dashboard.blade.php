@extends('admin.layouts.app')

  @section('content')
  <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Dashboard</h4>
                    </div>
                    <div class="content">
                        Dashboard
                    </div>
                </div>
              </div>
          </div>
      </div>
  </div>
  @endsection
