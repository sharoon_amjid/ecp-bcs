<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestDetail extends Model
{
  protected $fillable = [
      'user_id', 'test_id', 'obtained_marks',
  ];

  public function test()
  {
    return $this->belongsTo('App\Test');
  }
}
