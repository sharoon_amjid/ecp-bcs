<?php

namespace App\Traits;


trait Orderable
{
  public function scopeLatestFirst($query)
  {
    return $query->OrderBy('created_at', 'desc');
  }
}
