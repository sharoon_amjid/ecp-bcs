<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
  protected $fillable = [
      'user_id', 'batch_id', 'subject_id', 'semester_id',
  ];
}
