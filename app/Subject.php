<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'slug', 'display_name', 'user_id', 'semester_id',
  ];



  public function user()
  {
    return $this->belongsTo('App\User');
  }


  public function semester()
  {
    return $this->belongsTo('App\Semester');
  }

  public function getRouteKeyName()
  {
    return 'slug';
  }

  public static function registerSubjects()
  {
    Subject::Create([
      'slug' => 'programming-concept',
      'display_name' => 'Programming Concept',
      'semester_id' => '1'
    ]);

    Subject::Create([
      'slug' => 'calculas-I',
      'display_name' => 'Calculas I',
      'semester_id' => '1'
    ]);

    Subject::Create([
      'slug' => 'statistics',
      'display_name' => 'Statistics',
      'semester_id' => '1'
    ]);

    Subject::Create([
      'slug' => 'info-tech',
      'display_name' => 'Information Technology',
      'semester_id' => '1'
    ]);

    Subject::Create([
      'slug' => 'functional-english',
      'display_name' => 'Functional English',
      'semester_id' => '1'
    ]);

    Subject::Create([
      'slug' => 'physics',
      'display_name' => 'Physics',
      'semester_id' => '2'
    ]);

    Subject::Create([
      'slug' => 'calculas-II',
      'display_name' => 'Calculas II',
      'semester_id' => '2'
    ]);

    Subject::Create([
      'slug' => 'oop',
      'display_name' => 'OOP-Language I',
      'semester_id' => '2'
    ]);

    Subject::Create([
      'slug' => 'pak-studies',
      'display_name' => 'Pakistan Studies',
      'semester_id' => '2'
    ]);

    Subject::Create([
      'slug' => 'islamiyat',
      'display_name' => 'Islamiyat',
      'semester_id' => '2'
    ]);

    Subject::Create([
      'slug' => 'descrete-maths',
      'display_name' => 'Descrete Mathematics',
      'semester_id' => '2'
    ]);

    Subject::Create([
      'slug' => 'database-I',
      'display_name' => 'Database I',
      'semester_id' => '3'
    ]);

    Subject::Create([
      'slug' => 'digital-logic-design',
      'display_name' => 'Digital Login Design',
      'semester_id' => '3'
    ]);

    Subject::Create([
      'slug' => 'electronics',
      'display_name' => 'Electronics',
      'semester_id' => '3'
    ]);

    Subject::Create([
      'slug' => 'data-structure',
      'display_name' => 'Data Structure',
      'semester_id' => '3'
    ]);

    Subject::Create([
      'slug' => 'business-communication',
      'display_name' => 'Business Communication',
      'semester_id' => '3'
    ]);

    Subject::Create([
      'slug' => 'database-II',
      'display_name' => 'Database II',
      'semester_id' => '4'
    ]);

    Subject::Create([
      'slug' => 'vb-net',
      'display_name' => 'VB.Net Programming Language II',
      'semester_id' => '4'
    ]);

    Subject::Create([
      'slug' => 'data-communication-&-networking',
      'display_name' => 'Data Communication & Networking',
      'semester_id' => '4'
    ]);

    Subject::Create([
      'slug' => 'operating-system',
      'display_name' => 'Operating System',
      'semester_id' => '4'
    ]);

    Subject::Create([
      'slug' => 'assembly-language',
      'display_name' => 'Assembly Language',
      'semester_id' => '4'
    ]);

    Subject::Create([
      'slug' => 'algorithm',
      'display_name' => 'Algorithm',
      'semester_id' => '5'
    ]);

    Subject::Create([
      'slug' => 'software-engineering-I',
      'display_name' => 'Software Engineering I',
      'semester_id' => '5'
    ]);

    Subject::Create([
      'slug' => 'java',
      'display_name' => 'Java Programming Language III',
      'semester_id' => '5'
    ]);

    Subject::Create([
      'slug' => 'internet-programming',
      'display_name' => 'Internet Programming',
      'semester_id' => '5'
    ]);

    Subject::Create([
      'slug' => 'networking-strategies',
      'display_name' => 'Networking Strategies',
      'semester_id' => '5'
    ]);

    Subject::Create([
      'slug' => 'artificial-intelligence',
      'display_name' => 'Articial Intelligence',
      'semester_id' => '5'
    ]);

    Subject::Create([
      'slug' => 'numerical-analysis',
      'display_name' => 'Numerical Analysis',
      'semester_id' => '6'
    ]);

    Subject::Create([
      'slug' => 'software-engineering-II',
      'display_name' => 'Software Engineering II',
      'semester_id' => '6'
    ]);

    Subject::Create([
      'slug' => 'automata-theory',
      'display_name' => 'Automata Theory',
      'semester_id' => '6'
    ]);

    Subject::Create([
      'slug' => 'computer-graphics',
      'display_name' => 'Computer Graphics',
      'semester_id' => '6'
    ]);

    Subject::Create([
      'slug' => 'computer-architecture-design',
      'display_name' => 'Computer Architecture Design',
      'semester_id' => '6'
    ]);

    Subject::Create([
      'slug' => 'destributed-computing',
      'display_name' => 'Destributed Computing',
      'semester_id' => '7'
    ]);

    Subject::Create([
      'slug' => 'wireless-&-mobile-com',
      'display_name' => 'Wireles & Mobile Communication',
      'semester_id' => '7'
    ]);

    Subject::Create([
      'slug' => 'dataware-houses-&-data-mining',
      'display_name' => 'Datware Houses & Data Mining',
      'semester_id' => '7'
    ]);

    Subject::Create([
      'slug' => 'compiler-construction',
      'display_name' => 'Compiler Construction',
      'semester_id' => '7'
    ]);

    Subject::Create([
      'slug' => 'software-management',
      'display_name' => 'Software Management',
      'semester_id' => '8'
    ]);

    Subject::Create([
      'slug' => 'nlp',
      'display_name' => 'Natural Language Processing',
      'semester_id' => '8'
    ]);

    Subject::Create([
      'slug' => 'database-administrator',
      'display_name' => 'Database Administrator',
      'semester_id' => '8'
    ]);

    Subject::Create([
      'slug' => 'tele-communication',
      'display_name' => 'Tele Communication',
      'semester_id' => '8'
    ]);
  }

}
