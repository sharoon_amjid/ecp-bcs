<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('api/v1/login', 'Api\v1\Auth\LoginController@login');

Route::prefix('api/v1/teacher')->middleware('auth:api')->group(function() {
  Route::post('semesters', 'Api\v1\Teacher\SemesterController@semesterList');
  Route::post('subjects', 'Api\v1\Teacher\SubjectController@subjectList');
  Route::post('last-attendance', 'Api\v1\Teacher\AttendanceController@getLastAttendance');
  Route::post('students-for-attendance', 'Api\v1\Teacher\AttendanceController@getStudentForAttendance');
  Route::post('add-new-attendance', 'Api\v1\Teacher\AttendanceController@addNewAttendance');
  Route::post('last-attendance', 'Api\v1\Teacher\AttendanceController@getLastAttendance');
  Route::post('attendance-status', 'Api\v1\Teacher\AttendanceController@getAttendanceStatus');
  Route::post('update-last-attendance', 'Api\v1\Teacher\AttendanceController@updateLastAttendance');
  Route::get('feeds/comments', 'Api\v1\Teacher\FeedCommentController@index')->name('teacher.feeds.comments');
  Route::post('single-feed/comments', 'Api\v1\Teacher\FeedCommentController@getSingleFeed')->name('teacher.single.feed.comments');
});


















Route::prefix('admin')->group(function() {
  Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
  Route::get('/register-teacher', 'Admin\Registerations\RegisterTeacherController@show')->name('register.teacher');
  Route::post('/register-teacher', 'Admin\Registerations\RegisterTeacherController@store')->name('register.teacher.submit');
  Route::post('/register-semester', 'Admin\Registerations\RegisterSemesterController@store')->name('register.semester.submit');
  Route::post('/register-subject', 'Admin\Registerations\RegisterSubjectController@store')->name('register.subject.submit');
  Route::get('/register-student', 'Admin\Registerations\RegisterStudentController@show')->name('register.student');
  Route::post('/register-student', 'Admin\Registerations\RegisterStudentController@store')->name('register.student.submit');
  // Teacher info
  Route::get('/teacher-list', 'Admin\TeacherController@show')->name('teacher.list');
  Route::get('/teacher-with-subject', 'Admin\TeacherController@teacherWithSubject')->name('teacher.subject');
  // Subject Info
  Route::get('/subject-list', 'Admin\SubjectController@subjectList')->name('subject.list');
  Route::get('/manage-subject', 'Admin\SubjectController@show')->name('manage.subject');
  Route::post('/manage-subject', 'Admin\SubjectController@update')->name('manage.subject.submit');
  Route::put('/assign-subject/{subject}', 'Admin\SubjectController@assignSubjectToTeacher')->name('assign.subject.submit');
  Route::put('/unassign-subject/{subject}', 'Admin\SubjectController@UnassignSubjectFromTeacher')->name('unassign.subject.submit');
  // Batch info
  Route::post('/register-batch/{semester}', 'Admin\BatchController@registerBatch')->name('register.batch.submit');

  // Semester Info
  Route::get('/manage-semester', 'Admin\SemesterController@manageSemester')->name('manage.semester');
  Route::get('/semester-list', 'Admin\SemesterController@semesterList')->name('semester-list');
  Route::get('/semester-detail', 'Admin\SemesterController@semesterDetail')->name('semester-detail');
  Route::get('/free-semester', 'Admin\SemesterController@freeSemester')->name('free-semester');
  Route::get('/free-batch', 'Admin\SemesterController@freeBatch')->name('free-batch');

  Route::put('/batch-complete', 'Admin\SemesterController@batchComplete')->name('batch-complete-submit');
  Route::put('/batch-leave/{semester}', 'Admin\SemesterController@batchLeave')->name('batch-leave-submit');
  Route::put('/assign-batch-to-semester/{semester}', 'Admin\SemesterController@assignBatchToSemester')->name('assign-batch-to-semester-submit');

});

Route::prefix('teacher')->middleware(['auth','teacher'])->group(function() {
  Route::get('/', 'Teacher\TeacherController@index');
  Route::get('/feeds', 'Teacher\FeedController@index')->name('teacher.feeds');
  Route::get('/feeds-collection', 'Teacher\FeedController@getFeeds')->name('teacher.feeds.collection');
  Route::post('/feeds/{user}', 'Teacher\FeedController@store')->name('teacher.feeds.upload.submit');
  Route::get('feeds/comments', 'Teacher\FeedCommentController@index')->name('teacher.feeds.comments');
  Route::get('feeds/comments/{user_id}', 'Teacher\FeedCommentController@profileActivityFeeds')->name('teacher.feeds.comments.user');
  Route::post('feeds/{feed}/comments', 'Teacher\FeedCommentController@store')->name('teacher.feeds.comments.submit');
  Route::get('messages', 'Teacher\MessageController@index')->name('teacher.messages');
  Route::get('messages/conversation-list', 'Teacher\MessageController@conversationList')->name('teacher.conversation.list');
  Route::get('messages/{message_id}/conversation-reply', 'Teacher\MessageController@conversationReply')->name('teacher.conversation.reply');
  Route::post('messages/conversation-store', 'Teacher\MessageController@storeConversation')->name('teacher.conversation.store');
  Route::post('messages/get-user/{username}', 'Teacher\MessageController@getUserForMessage')->name('teacher.conversation.users');
  Route::post('messages/store-messaging', 'Teacher\MessageController@storeMessaging')->name('teacher.conversation.store.messaging');
  Route::get('/semesters', 'Teacher\SemesterController@index')->name('teacher.semesters');
  Route::post('{user_id}/semesters', 'Teacher\SemesterController@semesterList')->name('teacher.semesters.semesterlist');
  Route::post('subjects', 'Teacher\SubjectController@subjectList')->name('teacher.subjects');
  Route::post('students-for-attendance', 'Teacher\AttendanceController@getStudentForAttendance')->name('teacher.students.for.attendance');
  Route::post('add-new-attendance', 'Teacher\AttendanceController@addNewAttendance')->name('teacher.students.new.attendance');
  Route::post('add-marks', 'Teacher\TestController@addMarks')->name('teacher.students.marks');
  Route::post('test-list', 'Teacher\TestController@testList')->name('teacher.students.test.list');
  Route::post('test-detail', 'Teacher\TestController@testDetails')->name('teacher.students.test.details');
  Route::post('last-attendance', 'Teacher\AttendanceController@getLastAttendance')->name('teacher.last.attendance');
  Route::post('update-last-attendance', 'Teacher\AttendanceController@updatetLastAttendance')->name('teacher.update.last.attendance');
  Route::post('attendance-status', 'Teacher\AttendanceController@getAttendanceStatus')->name('teacher.attendance.status');
  Route::get('profile/{user}', 'Teacher\ProfileController@index')->name('teacher.profile');
  Route::post('change-password', 'Teacher\Auth\ChangePasswordController@changePassword')->name('teacher.change.password');
  Route::post('profile-information', 'Teacher\ProfileController@saveProfileInformation')->name('save.profile.information');
  Route::post('profile-pic', 'Teacher\ProfileController@saveProfilepic')->name('save.profile.pic');
});

Route::prefix('student')->middleware(['auth','student'])->group(function() {
  Route::get('/', 'Student\TeacherController@index');
  Route::get('/feeds', 'Student\FeedController@index')->name('student.feeds');
  Route::get('/feeds-collection', 'Student\FeedController@getFeeds')->name('student.feeds.collection');
  Route::post('/feeds/{user}', 'Student\FeedController@store')->name('student.feeds.upload.submit');
  Route::get('feeds/comments', 'Student\FeedCommentController@index')->name('student.feeds.comments');
  Route::get('feeds/comments/{user_id}', 'Student\FeedCommentController@profileActivityFeeds')->name('student.feeds.comments.user');
  Route::post('feeds/{feed}/comments', 'Student\FeedCommentController@store')->name('student.feeds.comments.submit');
  Route::get('messages', 'Student\MessageController@index')->name('student.messages');
  Route::get('messages/conversation-list', 'Student\MessageController@conversationList')->name('student.conversation.list');
  Route::get('messages/{message_id}/conversation-reply', 'Student\MessageController@conversationReply')->name('student.conversation.reply');
  Route::post('messages/conversation-store', 'Student\MessageController@storeConversation')->name('student.conversation.store');
  Route::post('messages/get-user/{username}', 'Student\MessageController@getUserForMessage')->name('student.conversation.users');
  Route::post('messages/store-messaging', 'Student\MessageController@storeMessaging')->name('student.conversation.store.messaging');
  Route::get('/semesters', 'Student\SemesterController@index')->name('student.semesters');
  Route::post('{user_id}/semesters', 'Student\SemesterController@semesterList')->name('student.semesters.semesterlist');
  Route::post('subjects', 'Student\SubjectController@subjectList')->name('student.subjects');
  Route::post('students-for-attendance', 'Student\AttendanceController@getStudentForAttendance')->name('student.students.for.attendance');
  Route::post('add-new-attendance', 'Student\AttendanceController@addNewAttendance')->name('student.students.new.attendance');
  Route::post('last-attendance', 'Student\AttendanceController@getLastAttendance')->name('student.last.attendance');
  Route::post('update-last-attendance', 'Student\AttendanceController@updatetLastAttendance')->name('student.update.last.attendance');
  Route::post('attendance-status', 'Student\AttendanceController@getAttendanceStatus')->name('student.attendance.status');
  Route::get('profile/{user}', 'Student\ProfileController@index')->name('student.profile');
  Route::post('change-password', 'Student\Auth\ChangePasswordController@changePassword')->name('student.change.password');
  Route::post('profile-information', 'Student\ProfileController@saveProfileInformation')->name('student.save.profile.information');
  Route::post('test-list', 'Student\TestController@testList')->name('teacher.students.test.list');
  Route::post('test-detail', 'Student\TestController@testDetails')->name('teacher.students.test.details');
});
